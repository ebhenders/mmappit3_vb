<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ColorPicker
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.pbFE = New System.Windows.Forms.PictureBox
        Me.tbFEr = New System.Windows.Forms.TextBox
        Me.tbFEg = New System.Windows.Forms.TextBox
        Me.tbFEb = New System.Windows.Forms.TextBox
        Me.tbColorLabel = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cbbMin = New System.Windows.Forms.ComboBox
        Me.cbbMax = New System.Windows.Forms.ComboBox
        CType(Me.pbFE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(88, 108)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 28)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(185, 108)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(80, 28)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'pbFE
        '
        Me.pbFE.Location = New System.Drawing.Point(288, 33)
        Me.pbFE.Name = "pbFE"
        Me.pbFE.Size = New System.Drawing.Size(95, 57)
        Me.pbFE.TabIndex = 1
        Me.pbFE.TabStop = False
        '
        'tbFEr
        '
        Me.tbFEr.AcceptsReturn = True
        Me.tbFEr.Location = New System.Drawing.Point(117, 70)
        Me.tbFEr.Name = "tbFEr"
        Me.tbFEr.Size = New System.Drawing.Size(51, 20)
        Me.tbFEr.TabIndex = 2
        Me.tbFEr.Text = "0"
        '
        'tbFEg
        '
        Me.tbFEg.AcceptsReturn = True
        Me.tbFEg.Location = New System.Drawing.Point(174, 70)
        Me.tbFEg.Name = "tbFEg"
        Me.tbFEg.Size = New System.Drawing.Size(51, 20)
        Me.tbFEg.TabIndex = 3
        Me.tbFEg.Text = "0"
        '
        'tbFEb
        '
        Me.tbFEb.AcceptsReturn = True
        Me.tbFEb.Location = New System.Drawing.Point(231, 70)
        Me.tbFEb.Name = "tbFEb"
        Me.tbFEb.Size = New System.Drawing.Size(51, 20)
        Me.tbFEb.TabIndex = 4
        Me.tbFEb.Text = "0"
        '
        'tbColorLabel
        '
        Me.tbColorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColorLabel.Location = New System.Drawing.Point(118, 37)
        Me.tbColorLabel.Name = "tbColorLabel"
        Me.tbColorLabel.Size = New System.Drawing.Size(163, 22)
        Me.tbColorLabel.TabIndex = 7
        Me.tbColorLabel.Text = "<color label>"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Min Value"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(60, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Max Value"
        '
        'cbbMin
        '
        Me.cbbMin.FormattingEnabled = True
        Me.cbbMin.Location = New System.Drawing.Point(7, 35)
        Me.cbbMin.Name = "cbbMin"
        Me.cbbMin.Size = New System.Drawing.Size(50, 21)
        Me.cbbMin.TabIndex = 13
        '
        'cbbMax
        '
        Me.cbbMax.FormattingEnabled = True
        Me.cbbMax.Location = New System.Drawing.Point(63, 35)
        Me.cbbMax.Name = "cbbMax"
        Me.cbbMax.Size = New System.Drawing.Size(50, 21)
        Me.cbbMax.TabIndex = 14
        '
        'ColorPicker
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(409, 144)
        Me.Controls.Add(Me.cbbMax)
        Me.Controls.Add(Me.cbbMin)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbColorLabel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbFEb)
        Me.Controls.Add(Me.tbFEg)
        Me.Controls.Add(Me.tbFEr)
        Me.Controls.Add(Me.pbFE)
        Me.Name = "ColorPicker"
        Me.Text = "Define Colors"
        CType(Me.pbFE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents pbFE As System.Windows.Forms.PictureBox
    Friend WithEvents tbFEr As System.Windows.Forms.TextBox
    Friend WithEvents tbFEg As System.Windows.Forms.TextBox
    Friend WithEvents tbFEb As System.Windows.Forms.TextBox
    Friend WithEvents tbColorLabel As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbbMin As System.Windows.Forms.ComboBox
    Friend WithEvents cbbMax As System.Windows.Forms.ComboBox
End Class
