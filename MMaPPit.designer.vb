<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MMaPPit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MMaPPit))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton
        Me.tsbFireEvents = New System.Windows.Forms.ToolStripButton
        Me.tsbZoomIn = New System.Windows.Forms.ToolStripButton
        Me.tsbFixedZoomIn = New System.Windows.Forms.ToolStripButton
        Me.tsbFixedZoomOut = New System.Windows.Forms.ToolStripButton
        Me.tsbFullExtent = New System.Windows.Forms.ToolStripButton
        Me.tsbPan = New System.Windows.Forms.ToolStripButton
        Me.tsbPrev = New System.Windows.Forms.ToolStripButton
        Me.tsbID = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.tsbClearSel = New System.Windows.Forms.ToolStripButton
        Me.tsbcbbStandPoly = New System.Windows.Forms.ToolStripComboBox
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel
        Me.tbCoords = New System.Windows.Forms.ToolStripTextBox
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton
        Me.ListView1 = New System.Windows.Forms.ListView
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Button1 = New System.Windows.Forms.Button
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.MapDocumentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadMapDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SaveThisMapAsDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadADifferentTimeSeriesFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FunctionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SwitchXYToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ColorsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditCurrentColorsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DefineNewColorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LoadOtherColorFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditFireBoundaryColorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.cbDrawEdge = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tbCurrWindInd = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblFileName = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.rtbAcres = New System.Windows.Forms.RichTextBox
        Me.cbFireEvents = New System.Windows.Forms.CheckBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.panelMap = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.ToolStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2, Me.tsbFireEvents, Me.tsbZoomIn, Me.tsbFixedZoomIn, Me.tsbFixedZoomOut, Me.tsbFullExtent, Me.tsbPan, Me.tsbPrev, Me.tsbID, Me.ToolStripButton1, Me.tsbClearSel, Me.tsbcbbStandPoly, Me.ToolStripLabel1, Me.tbCoords, Me.ToolStripButton3})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(836, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Add Dataset"
        '
        'tsbFireEvents
        '
        Me.tsbFireEvents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbFireEvents.Enabled = False
        Me.tsbFireEvents.Image = CType(resources.GetObject("tsbFireEvents.Image"), System.Drawing.Image)
        Me.tsbFireEvents.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbFireEvents.Name = "tsbFireEvents"
        Me.tsbFireEvents.Size = New System.Drawing.Size(23, 22)
        Me.tsbFireEvents.Text = "ToolStripButton4"
        Me.tsbFireEvents.ToolTipText = "Add Fire Event Overlay"
        '
        'tsbZoomIn
        '
        Me.tsbZoomIn.CheckOnClick = True
        Me.tsbZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbZoomIn.Image = CType(resources.GetObject("tsbZoomIn.Image"), System.Drawing.Image)
        Me.tsbZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbZoomIn.Name = "tsbZoomIn"
        Me.tsbZoomIn.Size = New System.Drawing.Size(23, 22)
        Me.tsbZoomIn.Text = "ToolStripButton1"
        Me.tsbZoomIn.ToolTipText = "Zoom In"
        '
        'tsbFixedZoomIn
        '
        Me.tsbFixedZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbFixedZoomIn.Font = New System.Drawing.Font("Cooper Black", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbFixedZoomIn.Image = CType(resources.GetObject("tsbFixedZoomIn.Image"), System.Drawing.Image)
        Me.tsbFixedZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbFixedZoomIn.Name = "tsbFixedZoomIn"
        Me.tsbFixedZoomIn.Size = New System.Drawing.Size(23, 22)
        Me.tsbFixedZoomIn.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.tsbFixedZoomIn.ToolTipText = "Zoom In"
        '
        'tsbFixedZoomOut
        '
        Me.tsbFixedZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbFixedZoomOut.Font = New System.Drawing.Font("Cooper Black", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsbFixedZoomOut.Image = CType(resources.GetObject("tsbFixedZoomOut.Image"), System.Drawing.Image)
        Me.tsbFixedZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbFixedZoomOut.Name = "tsbFixedZoomOut"
        Me.tsbFixedZoomOut.Size = New System.Drawing.Size(23, 22)
        Me.tsbFixedZoomOut.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.tsbFixedZoomOut.ToolTipText = "Zoom Out"
        '
        'tsbFullExtent
        '
        Me.tsbFullExtent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbFullExtent.Image = CType(resources.GetObject("tsbFullExtent.Image"), System.Drawing.Image)
        Me.tsbFullExtent.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbFullExtent.Name = "tsbFullExtent"
        Me.tsbFullExtent.Size = New System.Drawing.Size(23, 22)
        Me.tsbFullExtent.Text = "ToolStripButton1"
        Me.tsbFullExtent.ToolTipText = "Full Extent"
        '
        'tsbPan
        '
        Me.tsbPan.CheckOnClick = True
        Me.tsbPan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPan.Image = CType(resources.GetObject("tsbPan.Image"), System.Drawing.Image)
        Me.tsbPan.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPan.Name = "tsbPan"
        Me.tsbPan.Size = New System.Drawing.Size(23, 22)
        Me.tsbPan.Text = "Pan"
        '
        'tsbPrev
        '
        Me.tsbPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbPrev.Image = CType(resources.GetObject("tsbPrev.Image"), System.Drawing.Image)
        Me.tsbPrev.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbPrev.Name = "tsbPrev"
        Me.tsbPrev.Size = New System.Drawing.Size(23, 22)
        Me.tsbPrev.Text = "Previous Extent"
        '
        'tsbID
        '
        Me.tsbID.CheckOnClick = True
        Me.tsbID.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbID.Image = CType(resources.GetObject("tsbID.Image"), System.Drawing.Image)
        Me.tsbID.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbID.Name = "tsbID"
        Me.tsbID.Size = New System.Drawing.Size(23, 22)
        Me.tsbID.Text = "Identify"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Select by Poly ID"
        '
        'tsbClearSel
        '
        Me.tsbClearSel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbClearSel.Enabled = False
        Me.tsbClearSel.Image = CType(resources.GetObject("tsbClearSel.Image"), System.Drawing.Image)
        Me.tsbClearSel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbClearSel.Name = "tsbClearSel"
        Me.tsbClearSel.Size = New System.Drawing.Size(23, 22)
        Me.tsbClearSel.Text = "ToolStripButton3"
        Me.tsbClearSel.ToolTipText = "Clear Selected Features"
        '
        'tsbcbbStandPoly
        '
        Me.tsbcbbStandPoly.Name = "tsbcbbStandPoly"
        Me.tsbcbbStandPoly.Size = New System.Drawing.Size(121, 25)
        Me.tsbcbbStandPoly.Text = "<Overlay Field>"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(41, 22)
        Me.ToolStripLabel1.Text = "x,X,y,Y"
        '
        'tbCoords
        '
        Me.tbCoords.Name = "tbCoords"
        Me.tbCoords.Size = New System.Drawing.Size(150, 25)
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(43, 22)
        Me.ToolStripButton3.Text = "Resize"
        '
        'ListView1
        '
        Me.ListView1.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.ListView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListView1.AutoArrange = False
        Me.ListView1.Location = New System.Drawing.Point(3, 3)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(124, 240)
        Me.ListView1.SmallImageList = Me.ImageList1
        Me.ListView1.TabIndex = 16
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.SmallIcon
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.ImageList = Me.ImageList2
        Me.Button1.Location = New System.Drawing.Point(128, 626)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(28, 26)
        Me.Button1.TabIndex = 4
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "Play5.gif")
        Me.ImageList2.Images.SetKeyName(1, "Stop.gif")
        '
        'Button2
        '
        Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(79, 8)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(28, 26)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "<"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(159, 7)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(28, 26)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = ">"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Window:"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 617)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Display"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MapDocumentsToolStripMenuItem, Me.FunctionToolStripMenuItem, Me.ColorsToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(836, 24)
        Me.MenuStrip1.TabIndex = 28
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'MapDocumentsToolStripMenuItem
        '
        Me.MapDocumentsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoadMapDocumentToolStripMenuItem, Me.SaveThisMapAsDocumentToolStripMenuItem, Me.LoadADifferentTimeSeriesFileToolStripMenuItem})
        Me.MapDocumentsToolStripMenuItem.Name = "MapDocumentsToolStripMenuItem"
        Me.MapDocumentsToolStripMenuItem.Size = New System.Drawing.Size(43, 20)
        Me.MapDocumentsToolStripMenuItem.Text = "Map"
        '
        'LoadMapDocumentToolStripMenuItem
        '
        Me.LoadMapDocumentToolStripMenuItem.Name = "LoadMapDocumentToolStripMenuItem"
        Me.LoadMapDocumentToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.LoadMapDocumentToolStripMenuItem.Text = "Load Map Document"
        '
        'SaveThisMapAsDocumentToolStripMenuItem
        '
        Me.SaveThisMapAsDocumentToolStripMenuItem.Name = "SaveThisMapAsDocumentToolStripMenuItem"
        Me.SaveThisMapAsDocumentToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.SaveThisMapAsDocumentToolStripMenuItem.Text = "Save This Map as Document"
        '
        'LoadADifferentTimeSeriesFileToolStripMenuItem
        '
        Me.LoadADifferentTimeSeriesFileToolStripMenuItem.Enabled = False
        Me.LoadADifferentTimeSeriesFileToolStripMenuItem.Name = "LoadADifferentTimeSeriesFileToolStripMenuItem"
        Me.LoadADifferentTimeSeriesFileToolStripMenuItem.Size = New System.Drawing.Size(244, 22)
        Me.LoadADifferentTimeSeriesFileToolStripMenuItem.Text = "Load A Different Time Series File"
        '
        'FunctionToolStripMenuItem
        '
        Me.FunctionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SwitchXYToolStripMenuItem})
        Me.FunctionToolStripMenuItem.Name = "FunctionToolStripMenuItem"
        Me.FunctionToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.FunctionToolStripMenuItem.Text = "Function"
        '
        'SwitchXYToolStripMenuItem
        '
        Me.SwitchXYToolStripMenuItem.Name = "SwitchXYToolStripMenuItem"
        Me.SwitchXYToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.SwitchXYToolStripMenuItem.Text = "Switch X Y"
        '
        'ColorsToolStripMenuItem
        '
        Me.ColorsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditCurrentColorsToolStripMenuItem, Me.DefineNewColorToolStripMenuItem, Me.LoadOtherColorFileToolStripMenuItem, Me.EditFireBoundaryColorToolStripMenuItem})
        Me.ColorsToolStripMenuItem.Name = "ColorsToolStripMenuItem"
        Me.ColorsToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.ColorsToolStripMenuItem.Text = "Colors"
        '
        'EditCurrentColorsToolStripMenuItem
        '
        Me.EditCurrentColorsToolStripMenuItem.Name = "EditCurrentColorsToolStripMenuItem"
        Me.EditCurrentColorsToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.EditCurrentColorsToolStripMenuItem.Text = "Edit Current Colors"
        '
        'DefineNewColorToolStripMenuItem
        '
        Me.DefineNewColorToolStripMenuItem.Name = "DefineNewColorToolStripMenuItem"
        Me.DefineNewColorToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.DefineNewColorToolStripMenuItem.Text = "Define New Color"
        '
        'LoadOtherColorFileToolStripMenuItem
        '
        Me.LoadOtherColorFileToolStripMenuItem.Name = "LoadOtherColorFileToolStripMenuItem"
        Me.LoadOtherColorFileToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.LoadOtherColorFileToolStripMenuItem.Text = "Load Color File"
        '
        'EditFireBoundaryColorToolStripMenuItem
        '
        Me.EditFireBoundaryColorToolStripMenuItem.Enabled = False
        Me.EditFireBoundaryColorToolStripMenuItem.Name = "EditFireBoundaryColorToolStripMenuItem"
        Me.EditFireBoundaryColorToolStripMenuItem.Size = New System.Drawing.Size(202, 22)
        Me.EditFireBoundaryColorToolStripMenuItem.Text = "Edit Fire Boundary Color"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'cbDrawEdge
        '
        Me.cbDrawEdge.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbDrawEdge.AutoSize = True
        Me.cbDrawEdge.Location = New System.Drawing.Point(6, 634)
        Me.cbDrawEdge.Name = "cbDrawEdge"
        Me.cbDrawEdge.Size = New System.Drawing.Size(121, 17)
        Me.cbDrawEdge.TabIndex = 29
        Me.cbDrawEdge.Text = "Draw Display Edges"
        Me.cbDrawEdge.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label4.Location = New System.Drawing.Point(102, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(116, 14)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "<display>"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Map Display:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(162, 620)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(244, 35)
        Me.GroupBox1.TabIndex = 32
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.tbCurrWindInd)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(412, 620)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(193, 37)
        Me.GroupBox2.TabIndex = 33
        Me.GroupBox2.TabStop = False
        '
        'tbCurrWindInd
        '
        Me.tbCurrWindInd.Location = New System.Drawing.Point(113, 10)
        Me.tbCurrWindInd.Name = "tbCurrWindInd"
        Me.tbCurrWindInd.Size = New System.Drawing.Size(40, 20)
        Me.tbCurrWindInd.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(131, 679)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 13)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "File:"
        '
        'lblFileName
        '
        Me.lblFileName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblFileName.AutoSize = True
        Me.lblFileName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFileName.Location = New System.Drawing.Point(167, 678)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(0, 13)
        Me.lblFileName.TabIndex = 35
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 699)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 9)
        Me.Label6.TabIndex = 37
        Me.Label6.Text = "Stand ID:"
        '
        'rtbAcres
        '
        Me.rtbAcres.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbAcres.Location = New System.Drawing.Point(3, 3)
        Me.rtbAcres.Name = "rtbAcres"
        Me.rtbAcres.Size = New System.Drawing.Size(124, 297)
        Me.rtbAcres.TabIndex = 39
        Me.rtbAcres.Text = ""
        Me.rtbAcres.WordWrap = False
        '
        'cbFireEvents
        '
        Me.cbFireEvents.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbFireEvents.AutoSize = True
        Me.cbFireEvents.Enabled = False
        Me.cbFireEvents.Location = New System.Drawing.Point(6, 657)
        Me.cbFireEvents.Name = "cbFireEvents"
        Me.cbFireEvents.Size = New System.Drawing.Size(107, 17)
        Me.cbFireEvents.TabIndex = 40
        Me.cbFireEvents.Text = "Draw Fire Events"
        Me.cbFireEvents.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button4.Font = New System.Drawing.Font("Arial", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(50, Byte))
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button4.Location = New System.Drawing.Point(126, 651)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(15, 18)
        Me.Button4.TabIndex = 41
        Me.Button4.Text = "v"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button5.Location = New System.Drawing.Point(142, 651)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(15, 18)
        Me.Button5.TabIndex = 42
        Me.Button5.Text = "^"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button5.UseVisualStyleBackColor = True
        '
        'panelMap
        '
        Me.panelMap.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelMap.Location = New System.Drawing.Point(-1, 6)
        Me.panelMap.Name = "panelMap"
        Me.panelMap.Size = New System.Drawing.Size(696, 556)
        Me.panelMap.TabIndex = 2
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.ListView1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.rtbAcres)
        Me.SplitContainer1.Size = New System.Drawing.Size(126, 553)
        Me.SplitContainer1.SplitterDistance = 246
        Me.SplitContainer1.TabIndex = 43
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 52)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.SplitContainer1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.panelMap)
        Me.SplitContainer2.Size = New System.Drawing.Size(836, 562)
        Me.SplitContainer2.SplitterDistance = 133
        Me.SplitContainer2.TabIndex = 44
        '
        'MMaPPit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(836, 712)
        Me.Controls.Add(Me.SplitContainer2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.cbFireEvents)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblFileName)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cbDrawEdge)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(622, 487)
        Me.Name = "MMaPPit"
        Me.Text = "MMaPPit 3"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbZoomIn As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbFullExtent As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPan As System.Windows.Forms.ToolStripButton
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents tsbID As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbPrev As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbClearSel As System.Windows.Forms.ToolStripButton
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents MapDocumentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadMapDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaveThisMapAsDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cbDrawEdge As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblFileName As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents rtbAcres As System.Windows.Forms.RichTextBox
    Friend WithEvents tbCurrWindInd As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents tbCoords As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents FunctionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SwitchXYToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbFireEvents As System.Windows.Forms.ToolStripButton
    Friend WithEvents cbFireEvents As System.Windows.Forms.CheckBox
    Friend WithEvents ColorsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditCurrentColorsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadOtherColorFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditFireBoundaryColorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents panelMap As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents tsbFixedZoomIn As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbFixedZoomOut As System.Windows.Forms.ToolStripButton
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoadADifferentTimeSeriesFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbcbbStandPoly As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents DefineNewColorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
