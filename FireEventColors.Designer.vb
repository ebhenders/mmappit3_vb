<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FireEventColors
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.pbFE = New System.Windows.Forms.PictureBox
        Me.tbFEr = New System.Windows.Forms.TextBox
        Me.tbFEg = New System.Windows.Forms.TextBox
        Me.tbFEb = New System.Windows.Forms.TextBox
        Me.tbFOb = New System.Windows.Forms.TextBox
        Me.tbFOg = New System.Windows.Forms.TextBox
        Me.tbFOr = New System.Windows.Forms.TextBox
        Me.pbFO = New System.Windows.Forms.PictureBox
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.pbFE, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbFO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 225)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 28)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(126, 225)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(80, 28)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fire Event Color"
        '
        'pbFE
        '
        Me.pbFE.Location = New System.Drawing.Point(183, 13)
        Me.pbFE.Name = "pbFE"
        Me.pbFE.Size = New System.Drawing.Size(95, 57)
        Me.pbFE.TabIndex = 1
        Me.pbFE.TabStop = False
        '
        'tbFEr
        '
        Me.tbFEr.Location = New System.Drawing.Point(12, 50)
        Me.tbFEr.Name = "tbFEr"
        Me.tbFEr.Size = New System.Drawing.Size(51, 20)
        Me.tbFEr.TabIndex = 2
        Me.tbFEr.Text = "0"
        '
        'tbFEg
        '
        Me.tbFEg.Location = New System.Drawing.Point(69, 50)
        Me.tbFEg.Name = "tbFEg"
        Me.tbFEg.Size = New System.Drawing.Size(51, 20)
        Me.tbFEg.TabIndex = 3
        Me.tbFEg.Text = "0"
        '
        'tbFEb
        '
        Me.tbFEb.Location = New System.Drawing.Point(126, 50)
        Me.tbFEb.Name = "tbFEb"
        Me.tbFEb.Size = New System.Drawing.Size(51, 20)
        Me.tbFEb.TabIndex = 4
        Me.tbFEb.Text = "0"
        '
        'tbFOb
        '
        Me.tbFOb.Location = New System.Drawing.Point(123, 140)
        Me.tbFOb.Name = "tbFOb"
        Me.tbFOb.Size = New System.Drawing.Size(51, 20)
        Me.tbFOb.TabIndex = 11
        Me.tbFOb.Text = "0"
        '
        'tbFOg
        '
        Me.tbFOg.Location = New System.Drawing.Point(66, 140)
        Me.tbFOg.Name = "tbFOg"
        Me.tbFOg.Size = New System.Drawing.Size(51, 20)
        Me.tbFOg.TabIndex = 10
        Me.tbFOg.Text = "0"
        '
        'tbFOr
        '
        Me.tbFOr.Location = New System.Drawing.Point(9, 140)
        Me.tbFOr.Name = "tbFOr"
        Me.tbFOr.Size = New System.Drawing.Size(51, 20)
        Me.tbFOr.TabIndex = 9
        Me.tbFOr.Text = "0"
        '
        'pbFO
        '
        Me.pbFO.Location = New System.Drawing.Point(180, 103)
        Me.pbFO.Name = "pbFO"
        Me.pbFO.Size = New System.Drawing.Size(95, 57)
        Me.pbFO.TabIndex = 8
        Me.pbFO.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 16)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Fire Origin Color"
        '
        'FireEventColors
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.tbFOb)
        Me.Controls.Add(Me.tbFOg)
        Me.Controls.Add(Me.tbFOr)
        Me.Controls.Add(Me.pbFO)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbFEb)
        Me.Controls.Add(Me.tbFEg)
        Me.Controls.Add(Me.tbFEr)
        Me.Controls.Add(Me.pbFE)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FireEventColors"
        Me.Text = "FireEventColors"
        CType(Me.pbFE, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbFO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pbFE As System.Windows.Forms.PictureBox
    Friend WithEvents tbFEr As System.Windows.Forms.TextBox
    Friend WithEvents tbFEg As System.Windows.Forms.TextBox
    Friend WithEvents tbFEb As System.Windows.Forms.TextBox
    Friend WithEvents tbFOb As System.Windows.Forms.TextBox
    Friend WithEvents tbFOg As System.Windows.Forms.TextBox
    Friend WithEvents tbFOr As System.Windows.Forms.TextBox
    Friend WithEvents pbFO As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
