Module Module1
    'used in the mapper subroutine
    Public MapLoaded As Boolean
    'change this line for testing - test 1
    Public MapPolyFile As String 'polygon/hexagon map file with poly/hex ID, stand ID, adjacent polys and x/y coords of hex centroid
    Public HeaderFile As String 'contains information about which columns have X/Y information, Stand ID and poly ID
    Public FireEventFile As String 'SLINK, then period-by-period fire event index
    'Public PolyAttFile As String
    Public OutputByPeriodFile As String 'stand prescription file...contains # of Rx per stand
    Public ColorIndexFile As String 'color file...gives raw values, labels and RGB values...
    Public SelectedID() As String 'either stand or polygon
    Public NSelectedID As Integer

    Public NumAtts As Integer 'for the Stand Atts form
    Public AttName() As String 'name of each attribute
    Public Atts() As String 'for the Stand Atts form
    Public gStandID As String 'global Stand ID for Stand Atts form

    Public bCancel As Boolean
    Public bIDClosed As Boolean

    Public FireEventColor As Color = Color.DarkMagenta
    Public FireOriginColor As Color = Color.Red
    Public bColorSaved As Boolean
    Public ChangeColor As ColorInfo

    Public Unique_Values() As Single 'unique values found in the files
    Public NUniqueValues As Integer

    Public SelectorText As String 'header of the Map Selector form
    Structure ColorInfo
        'Structure of color values and labels...so that each color can be referenced by a number
        Dim LabelColorInd As Integer 'the unique value associated with this color
        Dim Color As Color
        'Dim ColumnName As String
        Dim LabelMinVal As Single 'minimum value of the label
        Dim LabelMaxVal As Single 'the maximum value of the label
        Dim LabelDispStr As String 'label to display in the legend

    End Structure
    Public Function StripQuotes(ByVal str As String) As String
        StripQuotes = str
        Dim s As String = ""
        For i As Integer = 0 To str.Length - 1
            If str.Substring(i, 1) <> Chr(34) Then s = s & str.Substring(i, 1)
        Next
        StripQuotes = s

    End Function
    Public Function FirstString(ByVal str As String, ByVal StopChar As String) As String
        FirstString = str
        Dim s As String = ""

        For i As Integer = 0 To str.Length - 1
            If str.Substring(i, 1) <> StopChar Then s = s & str.Substring(i, 1)
            If str.Substring(i, 1) = StopChar Then Exit For
        Next i
        FirstString = s
    End Function
    Public Function LastString(ByVal str As String, ByVal StartChar As String) As String
        LastString = str
        Dim s As String = ""
        Dim badd As Boolean = True
        Dim pos As Integer = str.Length
        For i As Integer = 0 To str.Length - 1
            pos = pos - 1
            If str.Substring(pos, 1) = StartChar Then
                badd = False
                Exit For
            End If
            s = str.Substring(pos, 1) & s
        Next i
        LastString = s
    End Function
    Public Function GetStrIndex(ByVal Array() As String, ByVal NSearchVals As String, ByVal CompareVal As String) As Integer
        GetStrIndex = -1 'default
        For j As Integer = 0 To NSearchVals 'Array.Length - 1
            If CompareVal = Array(j) Then
                GetStrIndex = j
                Exit Function
            End If
        Next
    End Function
    Public Function GetSingleIndex(ByVal Array() As Single, ByVal CompareVal As Single) As Integer
        GetSingleIndex = -1 'default
        For j As Integer = 1 To Array.Length - 1
            If Math.Round(CompareVal, 3) = Math.Round(Array(j - 1), 3) Then
                GetSingleIndex = j
                Exit Function
            End If
        Next
    End Function
    Public Function FName(ByVal FullName As String) As String
        Dim i As Integer
        Dim j As Integer
        Dim dummy As String
        dummy = FullName
        i = 0
        j = 0
        Do
            i = i + 1
            If dummy.Substring(Len(dummy) - i, 1) = "." Then j = i
            If dummy.Substring(Len(dummy) - i, 1) = "\" Or i = Len(dummy) Then Exit Do
        Loop
        If j = 0 Then
            FName = ""
        ElseIf i = Len(dummy) Then 'just file and extension
            FName = dummy.Substring((Len(dummy) - i), (i - (j)))
        Else 'full path and extension
            FName = dummy.Substring((Len(dummy) - i + 1), (i - (j + 1)))
        End If
    End Function
End Module
