Public Class MapSelector

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim arr() As String
        arr = Split(tbStandID.Text, ",")
        NSelectedID = arr.Length
        ReDim SelectedID(NSelectedID)
        For j As Integer = 1 To NSelectedID
            SelectedID(j) = arr(j - 1)
        Next
        'SelectedStandID = tbStandID.Text
        Me.Close()
        'Application.DoEvents()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Selector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblSelectorText.Text = SelectorText
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbStandID.KeyPress
        If e.KeyChar = Chr(13) Then 'Chr(13) is the Enter Key
            'Runs the Button1_Click Event
            Button1_Click(Me, EventArgs.Empty)
        End If
    End Sub
End Class