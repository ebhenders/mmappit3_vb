Public Class ColorPicker



    Private Sub FireEventColors_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        bColorSaved = False
        tbColorLabel.Text = ChangeColor.LabelDispStr
        cbbMin.Text = ChangeColor.LabelMinVal
        cbbMax.Text = ChangeColor.LabelMaxVal
        tbFEr.Text = ChangeColor.Color.R.ToString
        tbFEg.Text = ChangeColor.Color.G.ToString
        tbFEb.Text = ChangeColor.Color.B.ToString
        pbFE.BackColor = ChangeColor.Color

        For jcolor As Integer = 1 To NUniqueValues
            cbbMin.Items.Add(Unique_Values(jcolor))
            cbbMax.Items.Add(Unique_Values(jcolor))
        Next

    End Sub

    Private Sub tbFEr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEr.Enter, tbFEr.Leave
        ' Dim arr As Integer
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        ChangeColor.Color = Color.FromArgb(255, r, g, b)
        pbFE.BackColor = ChangeColor.Color
    End Sub

    Private Sub tbFEg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEg.Enter, tbFEg.Leave
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        ChangeColor.Color = Color.FromArgb(255, r, g, b)
        pbFE.BackColor = ChangeColor.Color
    End Sub

    Private Sub tbFEb_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFEb.Enter, tbFEb.Leave
        Dim r As Integer
        Dim g As Integer
        Dim b As Integer
        r = tbFEr.Text
        If r < 0 Then r = 0
        If r > 255 Then r = 255
        tbFEr.Text = r
        g = tbFEg.Text
        If g < 0 Then g = 0
        If g > 255 Then g = 255
        tbFEg.Text = g
        b = tbFEb.Text
        If b < 0 Then b = 0
        If b > 255 Then b = 255
        tbFEb.Text = b
        ChangeColor.Color = Color.FromArgb(255, r, g, b)
        pbFE.BackColor = ChangeColor.Color
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bCancel = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ChangeColor.LabelDispStr = tbColorLabel.Text
        ChangeColor.LabelMinVal = cbbMin.Text
        ChangeColor.LabelMaxVal = cbbMax.Text
        bCancel = False
        bColorSaved = True
        Me.Close()
    End Sub
    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If bColorSaved = False Then bCancel = True
    End Sub

    Private Sub pbFE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbFE.Click

        Dim MyDialog As New ColorDialog
        If MyDialog.ShowDialog = System.Windows.Forms.DialogResult.Cancel Then Return

        ChangeColor.Color = MyDialog.Color
        tbFEr.Text = ChangeColor.Color.R
        tbFEg.Text = ChangeColor.Color.G
        tbFEb.Text = ChangeColor.Color.B
        pbFE.BackColor = ChangeColor.Color
        MyDialog = Nothing
    End Sub
    Private Sub pbFE_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pbFE.MouseHover

        pbFE.Cursor = Cursors.Hand

    End Sub

    Private Sub cbbMin_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbMin.SelectedIndexChanged
        If CType(cbbMax.Text, Single) < CType(cbbMin.Text, Single) Then cbbMax.Text = cbbMin.Text
    End Sub

    Private Sub cbbMax_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbMax.SelectedIndexChanged
        If CType(cbbMax.Text, Single) < CType(cbbMin.Text, Single) Then cbbMax.Text = cbbMin.Text
    End Sub
End Class