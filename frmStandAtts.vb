Public Class frmStandAtts
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListViewAtts As System.Windows.Forms.ListView
    Friend WithEvents lblStandID As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.lblStandID = New System.Windows.Forms.Label
        Me.ListViewAtts = New System.Windows.Forms.ListView
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 22)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Attributes For Stand"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Cooper Black", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(97, 349)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(74, 40)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Close"
        '
        'lblStandID
        '
        Me.lblStandID.Font = New System.Drawing.Font("Cooper Black", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStandID.Location = New System.Drawing.Point(8, 30)
        Me.lblStandID.Name = "lblStandID"
        Me.lblStandID.Size = New System.Drawing.Size(232, 22)
        Me.lblStandID.TabIndex = 5
        '
        'ListViewAtts
        '
        Me.ListViewAtts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListViewAtts.Location = New System.Drawing.Point(12, 55)
        Me.ListViewAtts.Name = "ListViewAtts"
        Me.ListViewAtts.Size = New System.Drawing.Size(256, 288)
        Me.ListViewAtts.TabIndex = 17
        Me.ListViewAtts.UseCompatibleStateImageBehavior = False
        Me.ListViewAtts.View = System.Windows.Forms.View.Details
        '
        'frmStandAtts
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(291, 390)
        Me.Controls.Add(Me.ListViewAtts)
        Me.Controls.Add(Me.lblStandID)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmStandAtts"
        Me.Text = "Stand Attributes"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Selector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim i As Integer
        Dim str As String = ""

        ListViewAtts.BeginUpdate()
        ListViewAtts.Clear()

        lblStandID.Text = gStandID
        ListViewAtts.Columns.Add("Attribute Name", 90, HorizontalAlignment.Left)
        ListViewAtts.Columns.Add("Value", 120, HorizontalAlignment.Left)
        For i = 1 To NumAtts
            Dim item As New ListViewItem(AttName(i), 0)
            ListViewAtts.Items.Add(item)
            item.SubItems.Add(Atts(i))

        Next i
        ListViewAtts.EndUpdate()
        bIDClosed = False
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        bidclosed = True
        Me.Close()
    End Sub

    Public Sub UpdateID()
        Dim i As Integer
        Dim str As String = ""

        ListViewAtts.BeginUpdate()
        ListViewAtts.Clear()

        lblStandID.Text = gStandID
        ListViewAtts.Columns.Add("Attribute Name", 90, HorizontalAlignment.Left)
        ListViewAtts.Columns.Add("Value", 120, HorizontalAlignment.Left)
        For i = 1 To NumAtts
            Dim item As New ListViewItem(AttName(i), 0)
            ListViewAtts.Items.Add(item)
            item.SubItems.Add(Atts(i))

        Next i
        ListViewAtts.EndUpdate()
        bIDClosed = False
    End Sub
    Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        bIDClosed = True
        Me.Dispose()
    End Sub
End Class
